var server = require('./main'),
    util = require('util'),
    path = require('path'),
    argv = require('optimist').argv;

var help = [
    "usage: server [options]",
    "",
    "Runs the demon profile server",
    "options:",
    " -p            Port that you want the home server to run on [8888]",
    " -s, --setup   Indicates we should configure the database first [false]",
    " -a, --auth    Show the encoded token for HTTP Basic Auth or not [no]",
    " -h, --help    You're looking at it right now:D"
].join('\n');

if (argv.h || argv.help) {
    return util.puts(help);
}

var options = {
    port: process.env.PORT || argv.p || 8888,
    basicAuth: argv.a || argv.auth || null
};

server.start(options, function(err, server) {
    if (err) {
        return util.puts('Error starting the profile server: ' + err.message)
    }
    util.puts('Profile server is runnning');
});
