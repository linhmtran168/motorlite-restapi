var util = require('util'),
    http = require('http'),
    winston = require('winston'),
    service = require('./router'),
    database = require('./database'),
    helpers = require('./helpers');

exports.createServer = function(port, db) {
    var router = service.createRouter(db);

    var server = http.createServer(function(request, response) {
        var body = '';

        winston.info('Incoming Request', {url: request.url});

        request.on('data', function(chunk) {
            body += chunk;
        });

        request.on('end', function() {
            // Dispatch the request to the service
            var emitter = router.handle(request, body, function (route) {
                response.writeHead(route.status, route.headers);
                response.end(route.body);
            });

            emitter.on('log', function (info) {
                winston.info('Request completed', info);
            })
        });
    });

    if (port) {
        server.listen(port);
    }

    return server;
};

exports.start = function(options, callback) {
    database.setup(options, function (err, db) {
        if (err) {
            return callback(err);
        }

        // If -a is in the argument use HTTP Basic Auth
        if (options.basicAuth) {
            util.puts('Configuring HTTP Basic Auth. Base64 Encoded Username/Password: ' + helpers.base64.encode(helpers.auth.username + ':' + helpers.auth.password));
            // util.puts(helpers.auth.username+":"+helpers.auth.password);
        }

        callback(null, exports.createServer(options.port, db));
    });
}

