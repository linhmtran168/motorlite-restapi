var journey = require('journey');

// Endcoding helper
var base64 = {
    encode: function(unencoded) {
        return new Buffer(unencoded || '').toString('base64');
    },

    decode: function(encoded) {
        return new Buffer(encoded || '', 'base64').toString('utf8');
    }
};

// Authentication helper
var auth = {
    username: 'admin',
    password: 'password',
    basicAuth: function(request, body, callback) {
        var realm = "Authorization Required",
            authorization = request.headers.authorization;

        if (!authorization) {
            return callback(new journey.NotAuthorized("Authorization header is required"));
        }

        var parts = authorization.split(" ")
            scheme = parts[0],
            credentials = base64.decode(parts[1]).split(":");
            
        // console.log(credentials);

        if (scheme !== "Basic") {
            return callback(new journey.NotAuthorized("Authorization scheme must be basic"));
        } else if (!credentials[0] && !credentials[1]) {
            return callback(new journey.NotAuthorized("Both username and password are required"));
        } else if (credentials[0] !== auth.username || credentials[1] !== auth.password) {
            // console.log(credentials[0] + ':' + credentials[1]);
            return callback(new journey.NotAuthorized("Invalid username and password"));
        }

        callback(null);
    }
};

exports.base64 = base64;
exports.auth = auth;
