var journey = require('journey'),
    helpers = require('./helpers');

/**
 * Creates the RESTful router for the webservice
 */
 exports.createRouter = function(resource) {
     var router = new (journey.Router) ({
         strick: false,
         strickUrls: false,
         api: 'basic',
         filter: helpers.auth.basicAuth
     });

     router.path('/profiles', function() {
         // Authentication: Add a filter() method to perfrom HTTP Basic Auth
         this.filter(function() {
             // GET to /profiles lists all profiles
             this.get().bind(function(res) {
                 /* res.send(501, {}, {action: 'list'}); */
                 resource.getAll(function (err, profiles) {
                     if (err) {
                         return res.send(500, {}, {error: err.error});
                     }

                     res.send(200, {}, {profiles: profiles});
                 });
             });

             // GET to /profiles/top/:limit to show a number of top profiles
             this.get(/\/top\/(\d+)$/).bind(function(res, limit) {
                 resource.getTop(limit, function (err, profiles) {
                     if (err) {
                         return res.send(500, {}, {error: err.error});
                     }

                     res.send(200, {}, {profiles: profiles});
                 });
             });

             // GET to /profiles/:id shows the details of a profile
             this.get(/\/([\w|\d|\-|\_]+)/).bind(function(res, id) {
                 resource.getOne(id, function(err, profile) {
                     if (err) {
                         return res.send(500, {}, {error: err.error});
                     }

                     res.send(200, {}, {profile: profile});
                 });
             });

             // POST to /profiles creates a new profile
             this.post().bind(function(res, profile) {
                 // res.send(501, {}, {action: 'create'});
                 resource.createNew(profile, function(err, id) {
                     if (err) {
                         return res.send(500, {}, {error: err.error});
                     } 

                     res.send(200, {}, {profile_id: id});
                 });
             });

             // PUT to /profiles/:id updates an existing profile
             this.put(/\/([\w|\d|\-|\_]+)/).bind(function(res, id, profile) {
                 // res.send(501, {}, {action: 'update'});
                 resource.update(id, profile, function(err, result) {
                     if (err) {
                         return res.send(500, {}, {error: err.error});
                     }

                     if (result != 1) {
                         return res.send(500, {}, {error: "Couldn't update the profile"});
                     }

                     res.send(200, {}, {result: result});
                 })
             });

             // DELETE to /profiles/:id delete a specific profile
             this.del(/\/([\w|\d|\d\-|\_]+)/).bind(function(res, id) {
                 // res.send(501, {}, {action: 'delete'});
                 resource.delete(id, function(err, result) {
                     if (err) {
                         return res.send(500, {}, {error: err.error});
                     }

                     if (result != 1) {
                         return res.send(500, {}, {error: "Couldn't delete the profile"});
                     }

                     res.send(200, {}, {result: result});
                 });
             });
         });
     });

     return router;
 }
