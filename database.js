var mysql = require('mysql'),
    Client = require('mysql').Client,
    DATABASE = 'motorlite',
    TABLE = 'profile';

client = new Client();

/**
 * Connect to MySQL database
 */
function connect(callback) {
    client = mysql.createClient({
        user: 'root',
        password : 'root',
    });

    client.query("USE " + DATABASE, function(err, results) {
        if (err) {
            client.end();
            return callback(err);
        }
        
        console.log("Successfully connected to the database");
    });
}

/**
 * Create the object to hold sql interaction method
 */
function setup(options, callback) {
    var db = {
        getAll: function(callback) {
            connect(callback);
            client.query("SELECT * FROM " + TABLE, function (err, results, fields) {
                if (err) {
                    return callback(err);
                }

                // console.log(results);
                client.end();
                return callback(null, results);
            });
        },

        getTop: function(limit, callback) {
            connect(callback);
            client.query("SELECT * FROM " + TABLE + " ORDER BY win DESC,lose ASC, best_time ASC " +
                        "LIMIT ?", [limit], function(err, results) {
                if (err) {
                    console.log(err);
                    return callback(err);
                }

                client.end();
                return callback(null, results);
            });
        },

        getOne: function(id, callback){
            connect(callback);
            // Get the profile and its ranking
            client.query("SET @rank := 0");
            client.query("SELECT * FROM " +
                         "(SELECT *, @rank := @rank + 1 AS rank FROM " +
                         TABLE + " ORDER BY win DESC,best_time ASC) as result WHERE id = ?", [id], function(err, result) {
                if (err) {
                    console.log(err);
                    return callback(err);
                }

                // console.log(result);
                client.end();
                return callback(null, result);
            });
        },

        createNew: function(profile, callback) {
            connect(callback);
            client.query("INSERT INTO " + TABLE + " SET " +
                        "name = ?, win = ?, lose = ?, best_time = ?", 
                        [profile.name, profile.win, profile.lose, profile.best_time], function(err, info) {
                            if (err) {
                                return callback(err);
                            }

                            // console.log(info.insertId);
                            client.end();
                            return callback(null, info.insertId);
                        });
        },

        update: function(id, profile, callback) {
            connect(callback);
            client.query("UPDATE " + TABLE + " SET " +
                        "name = ?, win = ?, lose = ?, best_time = ? " +
                        "where id = ?", [profile.name, profile.win, profile.lose, profile.best_time, id], function(err, info) {
                if (err) {
                    return callback(err);
                }

                // console.log(info.affectedRows);
                client.end();
                return callback(null, info.affectedRows);
            });
        },

        delete: function(id, callback) {
            connect(callback);
            client.query("DELETE FROM " + TABLE +
                        " WHERE id = ?", [id], function(err, info) {
                if (err) {
                    return callback(err);
                }

                // console.log(info.affectedRows);
                client.end();
                return callback(null, info.affectedRows);
            });
        }

    };

    callback(null, db);
}

exports.setup = setup;
